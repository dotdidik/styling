import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Hello from '@/components/Hello'
import Fetchapi from '@/components/fetch-api/appHome'
import Login from '@/components/cms/appLogin'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/hello',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/fetch',
      name: 'Fetchapi',
      component: Fetchapi
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
